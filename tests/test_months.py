"Tests for SQL INTERVAL types based on MONTH"
import datetime as dt
from typing import Any, Callable

import pytest


def test_month(eval_sqlexpr: Callable[[str], Any]) -> None:
	c1 = eval_sqlexpr("INTERVAL -'88' MONTH")
	assert c1 == -88
	assert str(c1) == '-88'
	assert c1.sqlrepr() == "INTERVAL -'88' MONTH"


def test_year(eval_sqlexpr: Callable[[str], Any]) -> None:
	c1 = eval_sqlexpr("INTERVAL '10' YEAR")
	assert c1 == 120
	assert str(c1) == ' 10'
	assert c1.sqlrepr() == "INTERVAL '10' YEAR"


def test_year_to_month(eval_sqlexpr: Callable[[str], Any]) -> None:
	c1 = eval_sqlexpr("INTERVAL -'1-05' YEAR TO MONTH")
	assert c1 == -17
	assert str(c1) == '-1-05'
	assert c1.sqlrepr() == "INTERVAL -'1-05' YEAR TO MONTH"


def test_value(eval_sqlexpr: Callable[[str], Any]) -> None:
	assert eval_sqlexpr("INTERVAL -'88' MONTH").value == -88
	assert eval_sqlexpr("INTERVAL '10' YEAR").value == 10
	assert eval_sqlexpr("INTERVAL -'1-05' YEAR TO MONTH").value == (-1, -5)


def test_null(eval_sqlexpr: Callable[[str], Any]) -> None:
	c1 = eval_sqlexpr("CAST(NULL AS INTERVAL YEAR TO MONTH)")
	assert c1 is None


def test_addto_date(eval_sqlexpr: Callable[[str], Any]) -> None:
	assert eval_sqlexpr("INTERVAL '1-05' YEAR TO MONTH") + dt.date(2019, 8, 1) == dt.date(2021, 1, 1)
	assert (eval_sqlexpr("INTERVAL '1-05' YEAR TO MONTH") + eval_sqlexpr("INTERVAL '7' MONTH")).sqlrepr() == "INTERVAL '2-00' YEAR TO MONTH"


def test_add_invalid(eval_sqlexpr: Callable[[str], Any]) -> None:
	with pytest.raises(ValueError):
		eval_sqlexpr("INTERVAL '1' MONTH") + dt.date(2019, 1, 31)
