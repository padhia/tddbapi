"global setup including fixtures"
from typing import Any, Callable, Iterable

import pytest
from tdtypes import Cursor
from tdtypes.dbapi import cursor


@pytest.fixture(scope="session")
def dbcsr() -> Iterable[Cursor]:
	"returns a cursor as generator"
	with cursor(None) as csr:
		yield csr


@pytest.fixture(scope="session")
def eval_sqlexpr(dbcsr: Cursor) -> Callable[[str], Any]:
	"Return a function that evaluates SQL literal expression and returns the result"
	def wrapper(expr: str) -> Any:
		dbcsr.execute(f"SELECT {expr} AS C1")
		return dbcsr.fetchone()[0]

	return wrapper
