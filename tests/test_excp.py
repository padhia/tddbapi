"Test DB API Exceptions"
import pytest
from tddbapi import DataError, NotSupportedError, ProgrammingError
from tdtypes import Cursor


def test_3807(dbcsr: Cursor) -> None:
	with pytest.raises(ProgrammingError) as ex:
		dbcsr.execute("SELECT * FROM DBC.NON_EXISTING_TABLE")
	assert ex.value.sqlcode == 3807
	assert ex.value.sqltext == "Object 'DBC.NON_EXISTING_TABLE' does not exist."


def test_3706(dbcsr: Cursor) -> None:
	with pytest.raises(ProgrammingError) as ex:
		dbcsr.execute("SELECT * FRO DBC.DBCINFOV")
	assert ex.value.sqlcode == 3706
	assert ex.value.sqltext == "Syntax error: SELECT * must have a FROM clause."


def test_3932(dbcsr: Cursor) -> None:
	with pytest.raises(ProgrammingError) as ex:
		dbcsr.execute("database dbc;select * from dbc.dbcinfov")
	assert ex.value.sqlcode == 3932
	assert ex.value.sqltext == "Only an ET or null statement is legal after a DDL Statement."


def test_2618(dbcsr: Cursor) -> None:
	with pytest.raises(DataError) as ex:
		dbcsr.execute("select 1/0")
	assert ex.value.sqlcode == 2618
	assert ex.value.sqltext == "Invalid calculation:  division by zero."


def test_7453(dbcsr: Cursor) -> None:
	with pytest.raises(DataError) as ex:
		dbcsr.execute("select cast(100 as interval second)")
	assert ex.value.sqlcode == 7453
	assert ex.value.sqltext == "Interval field overflow."


def test_3532(dbcsr: Cursor) -> None:
	with pytest.raises(NotSupportedError) as ex:
		dbcsr.execute("select 'FF'xb c1, cast(c1 as int) c2")
	assert ex.value.sqlcode == 3532
	assert ex.value.sqltext == "Conversion between BYTE data and other types is illegal."
