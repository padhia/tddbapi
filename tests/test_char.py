"Test Python metadata for CHAR SQL Type"
from typing import Any, Callable


def test_char(eval_sqlexpr: Callable[[str], Any]) -> None:
	c1 = eval_sqlexpr("CAST('A' as CHAR(10))")
	assert len(c1) == 10
	assert c1 == 'A'.ljust(10)


def test_null(eval_sqlexpr: Callable[[str], Any]) -> None:
	c1 = eval_sqlexpr("CAST(NULL as CHAR(10))")
	assert c1 is None
