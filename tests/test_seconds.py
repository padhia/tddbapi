"Tests for SQL INTERVAL types based on SECOND"
from datetime import timedelta
from typing import Any, Callable


def test_second(eval_sqlexpr: Callable[[str], Any]) -> None:
	c1 = eval_sqlexpr("INTERVAL '65' SECOND")
	assert c1 == timedelta(seconds=65)
	assert c1 == timedelta(minutes=1, seconds=5)
	assert str(c1) == ' 65'
	assert c1.sqlrepr() == "INTERVAL '65' SECOND"


def test_second_frac(eval_sqlexpr: Callable[[str], Any]) -> None:
	c1 = eval_sqlexpr("INTERVAL '5.900' SECOND")
	assert c1 == timedelta(seconds=5, microseconds=900000)
	assert str(c1) == ' 5.900'
	assert c1.sqlrepr() == "INTERVAL '5.900' SECOND"


def test_minute(eval_sqlexpr: Callable[[str], Any]) -> None:
	c1 = eval_sqlexpr("INTERVAL '5' MINUTE")
	assert c1 == timedelta(minutes=5)
	assert str(c1) == ' 5'
	assert c1.sqlrepr() == "INTERVAL '5' MINUTE"


def test_minute_to_second(eval_sqlexpr: Callable[[str], Any]) -> None:
	c1 = eval_sqlexpr("INTERVAL -'5:30' MINUTE TO SECOND")
	assert c1 == timedelta(seconds=-330)
	assert str(c1) == '-5:30'
	assert c1.sqlrepr() == "INTERVAL -'5:30' MINUTE TO SECOND"


def test_hour(eval_sqlexpr: Callable[[str], Any]) -> None:
	c1 = eval_sqlexpr("INTERVAL '5' HOUR")
	assert c1 == timedelta(hours=5)
	assert str(c1) == ' 5'
	assert c1.sqlrepr() == "INTERVAL '5' HOUR"


def test_hour_to_minute(eval_sqlexpr: Callable[[str], Any]) -> None:
	c1 = eval_sqlexpr("INTERVAL '5:30' HOUR TO MINUTE")
	assert c1 == timedelta(hours=5, minutes=30)
	assert str(c1) == ' 5:30'
	assert c1.sqlrepr() == "INTERVAL '5:30' HOUR TO MINUTE"


def test_hour_to_second(eval_sqlexpr: Callable[[str], Any]) -> None:
	c1 = eval_sqlexpr("INTERVAL '5:30:20' HOUR TO SECOND")
	assert c1 == timedelta(hours=5, minutes=30, seconds=20)
	assert str(c1) == ' 5:30:20'
	assert c1.sqlrepr() == "INTERVAL '5:30:20' HOUR TO SECOND"


def test_day(eval_sqlexpr: Callable[[str], Any]) -> None:
	c1 = eval_sqlexpr("INTERVAL '2' DAY")
	assert c1 == timedelta(days=2)
	assert str(c1) == ' 2'
	assert c1.sqlrepr() == "INTERVAL '2' DAY"


def test_day_to_hour(eval_sqlexpr: Callable[[str], Any]) -> None:
	c1 = eval_sqlexpr("INTERVAL '1 05' DAY TO HOUR")
	assert c1 == timedelta(days=1, hours=5)
	assert str(c1) == ' 1 05'
	assert c1.sqlrepr() == "INTERVAL '1 05' DAY TO HOUR"


def test_day_to_minute(eval_sqlexpr: Callable[[str], Any]) -> None:
	c1 = eval_sqlexpr("INTERVAL '1 05:30' DAY TO MINUTE")
	assert c1 == timedelta(days=1, hours=5, minutes=30)
	assert str(c1) == ' 1 05:30'
	assert c1.sqlrepr() == "INTERVAL '1 05:30' DAY TO MINUTE"


def test_day_to_second(eval_sqlexpr: Callable[[str], Any]) -> None:
	c1 = eval_sqlexpr("INTERVAL '3 05:30:20' DAY TO SECOND")
	assert c1 == timedelta(days=3, hours=5, minutes=30, seconds=20)
	assert str(c1) == ' 3 05:30:20'
	assert c1.sqlrepr() == "INTERVAL '3 05:30:20' DAY TO SECOND"


def test_null(eval_sqlexpr: Callable[[str], Any]) -> None:
	c1 = eval_sqlexpr("CAST(NULL AS INTERVAL DAY TO SECOND)")
	assert c1 is None
